import argparse

import re
from platform_tools.platformdata import post_data
from geojson import Point, FeatureCollection, LineString, Feature

def parse_line(line):
    if (line == 'START\n') or (line == 'END\n'):
        pass #print(line)
    else:
        dic_line ={}
        # Get WP, Lat, Long, Distance to next, and Heading to next.
        split_line = line.split('; ', maxsplit=5)
        dic_line['WP']    = split_line.pop(0)
        dic_line['Lat']   = float(split_line.pop(0))
        dic_line['Long']  = float(split_line.pop(0))
        dic_line['Dist']  = split_line.pop(0)
        dic_line['Head']  = split_line.pop(0)


        # Get Depth from Surface as D### or Height from Bottom as H###.
        # Using .split(None) because inconsistant whitespace between values.
        # Depth and Height values read in FEET converted to meters below.

        split_line = split_line[0].split(None)
        depth_height = split_line.pop(0)

        if depth_height[0] == 'D':
            # Depth from Surface detected.
            dic_line['DFS'] = float(depth_height[1:]) * 0.3048
            dic_line['HFB'] = float(0.0)

        elif depth_height[0] == 'H':
            # Height from Bottom detected.
            dic_line['HFB'] = float(depth_height[1:]) * 0.3048
            dic_line['DFS'] = float(0.0)

        else:
            print('ERROR, depth type not found.')

        # Get Parking time in minutes for waypoint.
        dic_line['Park'] = float(split_line.pop(0)[1:])

        # Get Speed in knots to next waypoint.
        dic_line['Speed'] = float(split_line.pop(2)[1:-1])

        return dic_line

    return None

        # prints for debugging.
        #print(dic_line)
        #print(split_line)

def read_mission(fname):
    grab_line = False
    waypoint_list = []
    list_of_dics  = []

    with open(fname) as missison_file:
        for line in missison_file:

            if re.match('START', line):
                grab_line = True

            if grab_line:
                parsed_return = parse_line(line)

                if parsed_return is not None:
                    list_of_dics.append(parsed_return)
                    waypoint_list.append([parsed_return['Long'], parsed_return['Lat']])

            if re.match('END', line):
                break

    return list_of_dics, waypoint_list


def mission_to_geojson(fname, color="grey"):
    waypoints, coords = read_mission(fname)

    feature_list = []
    feature_list.append(Feature(geometry=LineString(coords), properties={
        # Styling options
        "options": {"color": color, "weight": 2, "opacity": 0.5, "dashArray": "5,5"},
        # Info fields are fully customisable. Key-value pairs are displayed in a popover when clicked
        "info": {"Type": "LineString: Path", "Name": "UMICH Iver3 Mission Path"}
    }))

    for wp in waypoints:
        point = Point([wp.pop("Long"), wp.pop("Lat")])
        feature_list.append(Feature(geometry=point, properties={
            # Styling options
            "options": {"radius": 3, "fillColor": color, "color": "black", "weight": 1, "opacity": 0.5, "fillOpacity": 0.5},
            # Info fields are fully customisable. Key-value pairs are displayed in a popover when clicked
            "info": wp
        }))

    return FeatureCollection(feature_list)


def main():

    # Parse input args
    parser = argparse.ArgumentParser()
    parser.add_argument('mission_path', action="store", type=str, help="Full path to mission file")
    parser.add_argument("-k", "--platform_key", default="umich.iver", type=str, help="Platform key to post to maptracker")
    parser.add_argument("-c", "--color", default="red", type=str, help="Colour to plot mission")
    parser.add_argument("-p", "--port", default=8080, type=int, help="Port for maptracker server")
    parser.add_argument("-u", "--url", default="http://10.23.9.150", type=str, help="URL for maptracker server (format: http://ipaddress)")

    # Set options
    args = parser.parse_args()

    mission_data = mission_to_geojson(args.mission_path, color=args.color)

    post_data(args.platform_key, mission_data, data_type="mission", maptracker_port=args.port, maptracker_url=args.url)
    # print (mission_data)

if __name__ == "__main__":
    main()

