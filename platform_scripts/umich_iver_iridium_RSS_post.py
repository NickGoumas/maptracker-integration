

import feedparser

def get_last_RSS(url):
    d = feedparser.parse(url)
    RSS_body = d.entries[0].description

    return RSS_body

def create_dict(message_body):
    features_dict = {}
    sliced_list = message_body.split(', ')
    header_list = ['Lat',
                   'Long',
                   'Speed',
                   'Head',
                   'WP',
                   'Bat',
                   'Act',
                   'Error',
                   'Mode',
                   ]

    for i in range(0, len(header_list)):

        sliced_element = sliced_list[i+1].split('=')[1]

        if sliced_element.isdigit():
            sliced_element = float(sliced_element)

        features_dict[header_list[i]] = sliced_element

    return features_dict


message_body = get_last_RSS('https://zapier.com/engine/rss/2895871/dropiveriridium/')

print(create_dict(message_body))