######################################################################
# This file handles the mission path POSTs to Maptracker.
# 
# Run the script with the misson file (full path) and the name of the
# configuration that the mission should be loaded to as 
# command line options and the mission is forwarded to Maptracker 
# as an HTTP POST message. 
# Maptracker will display the mission path until restart, or an 
# updated post to the same config.
#
######################################################################
import argparse
import time
import threading
import sys
import math
import select
import os
import collections
import requests

import pyproj  
from flask import Flask, render_template  
from geojson import Point, FeatureCollection, LineString, Feature, Polygon

import lcm

LCMROOT ='/home/auv/git/acfr-lcm'
sys.path.append('{0}/build/lib/python{1}.{2}/dist-packages/perls/lcmtypes/'.format(LCMROOT, sys.version_info[0], sys.version_info[1]))

try:
    import missionMP # for Sirus Missions
except:
    print 'Missing missionMP support'

try:
    import missionXML # for LCM Missions
except:
    print 'Missing missionXML support'

from platform_tools.platformdata import post_data


def mission_to_geojson(filepath, platformmission, plot_color):

    fileName, fileExtension = os.path.splitext(filepath)
    if fileExtension.lower() == '.xml':
        mission = missionXML.Mission()
        startind = 0
    elif fileExtension.lower() == '.mp':
        mission = missionMP.Mission()
        startind = 2
    elif fileExtension.lower() == '.mis':
        mission = missionUVC.Mission()
    else:
        print 'Incorrect mission type'
        return

    if os.path.exists(filepath):
        mission.load(filepath)
    else:
        print "Mission does not exist."
        return
    
    # Note: Reverse order because GEOJSON uses longlat ordering not latlong
    origin = [mission.getOriginLon(), mission.getOriginLat()]

    # UVC missions are already in Lat / Long
    if fileExtension.lower() == '.mis':
        latlngs = mission.dumpSimple()
    else:
        waypoints = mission.dumpSimple()
        # convert the waypoints to latitude and longitude
        projStr = '+proj=tmerc +lon_0={} +lat_0={} +units=m'.format(origin[0], origin[1])
        p = pyproj.Proj(projStr)

        latlngs = []
        for wpt in waypoints:
            ll = p(wpt.y, wpt.x, inverse=True)
            latlng = [ll[1], ll[0]]
            latlngs.append(latlng)

        latlngs = latlngs[startind:]
    
    lnglats = latlngs
    for i in lnglats:
        i.reverse()
    #print "Lat Long: " + str(latlngs)

    mission = FeatureCollection([
        Feature(geometry=Point(origin),properties={
            # Styling options
            "options": {"radius": 6, "fillColor": str(plot_color), "color": "black", "weight": 2, "opacity": 0.5, "fillOpacity": 0.5},
            # Info fields are fully customisable. Key-value pairs are displayed in a popover when clicked
            "info": {"Type": "Point: Origin", "Name": platformmission}
        }),
        Feature(geometry=Point(lnglats[0]),properties={
            # Styling options
            "options": {"radius": 6, "fillColor": str(plot_color), "color": "white", "weight": 1, "opacity": 1, "fillOpacity": 0.8},
            # Info fields are fully customisable. Key-value pairs are displayed in a popover when clicked
            "info": {"Type": "Point: START", "Name": platformmission}
        }),         
        Feature(geometry=Point(lnglats[-1]),properties={
            # Styling options
            "options": {"radius": 6, "fillColor": str(plot_color), "color": "black", "weight": 1, "opacity": 1, "fillOpacity": 0.8},
            # Info fields are fully customisable. Key-value pairs are displayed in a popover when clicked
            "info": {"Type": "Point: END", "Name": platformmission}
        }), 
        Feature(geometry=LineString(lnglats),properties={
            # Styling options
            "options": {"color": str(plot_color), "weight": 3, "opacity": 0.5, "dashArray": "5,5"},
            # Info fields are fully customisable. Key-value pairs are displayed in a popover when clicked
            "info": {"Type": "LineString: Path", "Name": platformmission, "Duration": "TBA"}
        })
	# NOTE: Can also include a Polygon features in a similar way        
    ])

    return mission

#--------------------------------------MAIN-------------------------------------------#

def Main():

    # Parse input args
    parser = argparse.ArgumentParser()
    parser.add_argument('mission_path', action="store", type=str, help="Full path to mission file")
    parser.add_argument('platform_key', action="store", type=str, help="Platform key for loading mission")
    parser.add_argument("-c", "--color", default="yellow", type=str, help="Colour to plot mission")
    parser.add_argument("-p", "--port", default=8080, type=int, help="Port for maptracker server")
    parser.add_argument("-u", "--url", default="http://localhost", type=str, help="URL for maptracker server (format: http://url)")

    # Set options
    args = parser.parse_args()
    mission_filename_path = args.mission_path
    platform_key = args.platform_key
    plot_color = args.color
    
    # write the input values to output for debug
    (mission_path, mission_name) = os.path.split(mission_filename_path)
    (filename, file_extension) = os.path.splitext(mission_name)

    print "\nLoading Mission: " + mission_name
    print "From path: " + mission_path
    print "Into Maptracker mission config: " + platform_key + "\n"

    # read and parse the mission file, checking suffix to confirm file type
    mission_data = mission_to_geojson(mission_filename_path, mission_filename_path, plot_color) # note: origin only comes from mission now

    # post the mission to maptracker
    if mission_data != {}:
        post_data(platform_key, mission_data, data_type="mission", maptracker_port=args.port, maptracker_url=args.url)
    
    # delay to get response 
    time.sleep(1)
    
    

#----------------------------------END-MAIN-------------------------------------------#

if __name__ == "__main__":
    Main()


