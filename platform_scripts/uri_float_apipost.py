######################################################################
# This file handles all the platform pose updates.
#
# An LCM thread listens for incoming messages and they are forwarded
# to Maptracker as HTTP POST messages.
#
#
######################################################################

from __future__ import division

import select
from functools import partial
from datetime import *
import signal
import os
import math
import lcm  # third party library
import sys, time, pdb
import string
from time import strftime, gmtime
import collections
from pyproj import Proj
import numpy as np

import ipdb

import threading
import math
import select
import os
import lcm

sys.path.append(os.path.join(os.path.dirname(__file__), '../../build/lcmtypes/python'))
from floatlcm import *
from general import *

from senlcm import *

from platform_tools.platformdata import APIPlatform, get_args


# --------------------------------------LCM-THREAD------------------------------------- #

class LcmThread(threading.Thread, APIPlatform):
    def __init__(self, **kwargs):
        threading.Thread.__init__(self)
        self.lc = lcm.LCM()
        self.exitFlag = False
        self.daemon = True  # run in daemon mode to allow for ctrl+C exit
        APIPlatform.__init__(self, **kwargs)

    def usbl_fix_handler(self, channel, data):
        """Handler to decode USBL message"""
        try:
            msg = usbl_fix_t.decode(data)
        except ValueError:
            print("invalid incoming usbl message")
            return

        print "unix time %d" % sec_time(msg.utime)

        xyt = xyt_t()
        x, y = self.projection(180 / math.pi * msg.longitude, 180 / math.pi * msg.latitude)
        # x,y = self.projection(msg.longitude*180/math.pi,msg.latitude);
        print"Got a fix: lon %3.4f lat %3.4f, x %5.1f y %5.1f" % (
            180 / math.pi * msg.longitude, 180 / math.pi * msg.latitude, x, y)
        xyt.x = x
        xyt.y = y
        xyt.t = (msg.utime / 1000000)
        self.xyt.append(xyt)

        self.new_crumb = False
        if (self.last_crumb_distance > self.crumb_distance):
            self.bread_crumb.t = sec_time(msg.utime)
            self.bread_crumb.x = x
            self.bread_crumb.y = y
            self.new_crumb = True
            print "----------------- Dropping a new bread crumb -------"
            self.leftright = not self.leftright
            self.crumb_hdg_cart = self.hdg_cart

        self.filter()

        self.goto_points()

        platform_data = {
            # All alert key-value pairs are optional. For no alerts, include empty dict, ie {}
            # The keys can be anything, but keep them short for display.
            # The values need to be int 0/1, with 1=on, 0=off.
            "alert": {
            },

            # required: lat, lon
            # optional, but required for dash display: heading, roll, pitch, bat
            # optional, speed, uncertainty, alt, depth
            # all values must be floats
            "pose": {
                "lat": lat,  # float, decimal degrees
                "lon": lon,  # float, decimal degrees
                "depth": round(msg.depth, 1),  # float, m
                "uncertainty": round(msg.accuracy, 2)  # float, m
            },

            # All stat key-value pairs are optional. For no stats, include empty dict, ie {}
            # optional, but required for dash display: bat
            # The keys can be anything, but keep them short for display.
            # The values can be anything, with the exception of bat, which must be an int
            "stat": {
                'XYZ': "{}, {}, {}".format(round(msg.target_x, 1), round(msg.target_y, 1), round(msg.target_z, 1))
            }
        }

        # Use the LCM Channel name as the platform_key so can tell which vehicle the status came from
        # This should match the url field in the config json file for the vehicle
        platform_key = str(channel)

        # And POST the data
        # post_platformdata(platform_key, platform_data, data_type="platform")
        self.update_platform_data(platform_key, platform_data)

    def goto_points(self):
        # calc distance from last crumb
        dist_vec = np.array([self.xyt[-1].x, self.xyt[-1].y]) - np.array([self.bread_crumb.x, self.bread_crumb.y])
        self.last_crumb_distance = math.hypot(dist_vec[0], dist_vec[1])

        # do if get a new crumb
        if self.new_crumb:
            print "------------------- New goto point --------------------"
            if self.leftright:
                ang = (self.crumb_hdg_cart - 90) * math.pi / 180
            else:
                ang = (self.crumb_hdg_cart + 90) * math.pi / 180

            self.offset.x = math.cos(ang) * self.crumb_offset
            self.offset.y = math.sin(ang) * self.crumb_offset

            self.goto.x = self.bread_crumb.x + self.offset.x
            self.goto.y = self.bread_crumb.y + self.offset.y

            print "################### New goto offset x = %5.1f,y = %5.1f ##########################" % (
                math.cos(ang) * self.crumb_offset, math.sin(ang) * self.crumb_offset)
            print "################### New goto point x = %5.1f, y=%5.1f, left/right %d #############" % (
                self.goto.x, self.goto.y, self.leftright)

            # convert the waypoints to latitude and longitude
            # projStr = '+proj=tmerc +lon_0={} +lat_0={} +units=m'.format(origin.lon, origin.lat)
            # p = pyproj.Proj(projStr)
            latlngs = []

            # for wpt in waypoints:
            ll = self.projection(self.goto.x, self.goto.y, inverse=True)
            #	latlng = [ll[1], ll[0]]
            #	latlngs.append(latlng
            self.fix_to_publish.latitude = ll[1] * math.pi / 180
            self.fix_to_publish.longitude = ll[0] * math.pi / 180
            print "---------------- Publishing goto lat %7.5f, lon %7.5f ----------------" % (ll[1], ll[0])
            self.lc.publish("USBL_FIX.GOTO", self.fix_to_publish.encode())

        print "---------------- Distance from last bread crumb - %4.1f -------" % (self.last_crumb_distance)
        print "---------------- Last goto pt x %5.1f, y %5.1f ----------------" % (self.goto.x, self.goto.y)
        print "---------------- Last offset x %5.1f, y %5.1f -----------------" % (self.offset.x, self.offset.y)

    def filter(self):
        # since we have a new message, try and run the filter
        if (len(self.xyt) >= self.buffer_length):
            # if(self.fix_time[-1] - self.fix_time[-self.buffer_length] < self.time_window):
            if (self.xyt[-1].t - self.xyt[-self.buffer_length].t < self.time_window):
                print "Calculating velocity"
                # for fix in self.xyt:
                vels = []

                # print fixes to screen
                for i in range(1, self.buffer_length + 1):
                    print "Fix %d: x %6.1f y %6.1f, DT from most recent %5.1d sec" % (
                        i, self.xyt[-i].x, self.xyt[-i].y, (self.xyt[-1].t - self.xyt[-i].t))

                # calc velocity
                for i in range(1, self.buffer_length):
                    vels.append(np.array([(self.xyt[-1].x - self.xyt[-i - 1].x) / (self.xyt[-1].t - self.xyt[-i - 1].t),
                                          (self.xyt[-1].y - self.xyt[-i - 1].y) / (
                                              self.xyt[-1].t - self.xyt[-i - 1].t)]))

                # print velocities to screen
                # for vel in vels:
                #	print "vx %4.2f, vy %4.2f" % (vel[0],vel[1])

                vel_sum = np.array([0, 0])
                # ipdb.set_trace()
                for vel in vels:
                    vel_sum = vel_sum + vel
                vel = vel_sum / (self.buffer_length - 1)

                # ipdb.set_trace()
                print "Avg vel: vx %4.2f, vy %4.2f" % (vel[0], vel[1])

                speed = math.hypot(vel[0], vel[1])
                self.hdg_cart = math.atan2(vel[1], vel[0]) * 180 / math.pi
                hdg_comp = (-self.hdg_cart + 90 + 360) % 360
                print "Speed %4.2f m/s - Direction of drift %5.1f" % (speed, hdg_comp)

            else:
                print "--Buffer times not within the %d second window, %6.1f sec" % (
                    self.time_window, self.xyt[-1].t - self.xyt[-self.buffer_length].t)
                for i in range(1, self.buffer_length + 1):
                    print "Fix %d: x %6.1f y %6.1f, DT from most recent %5.1d sec" % (
                        i, self.xyt[-i].x, self.xyt[-i].y, (self.xyt[-1].t - self.xyt[-i].t))

        else:
            print "--Waiting for buffer to fill, got fix %d of %d" % (len(self.xyt), self.buffer_length)

    def float_status_handler(self, channel, data):
        """Handle command sent to controller"""
        try:
            msg = acomm_status_t.decode(data)
        except ValueError:
            print("invalid incoming status message")
            return
        self.acomm_status = msg
        # print('got status message')

        self.last_status_time = time.time()

        # print controller cmd

        sys.stdout.write(
            "\n\nSTATUS: Dep %3.1f, alt %3.1f, ctl mode %d, ctl status % d, vol cmd %3d, vol act %3d, thr %4.2f, leg time %s, mis time %s, leg num %d, mis stat %d, leg type %d, bat %d\n\n" % (
                msg.depth / 10,
                msg.altitude / 10,
                msg.ctl_mode,
                msg.ctl_status,
                msg.volume_cmd,
                msg.volume_actual,
                msg.thrustI_cmd / 10,
                # (msg.leg_time),
                # (msg.mis_time),
                strftime("%H:%M:%S", gmtime(msg.leg_time)),
                strftime("%H:%M:%S", gmtime(msg.mis_time)),
                msg.leg_num,
                msg.mis_status,
                msg.leg_type,
                msg.bat_charge))

        sys.stdout.flush()

        platform_data = {
            # All alert key-value pairs are optional. For no alerts, include empty dict, ie {}
            # The keys can be anything, but keep them short for display.
            # The values need to be int 0/1, with 1=on, 0=off.
            "alert": {
                "dvl": bool(msg.status & (1 << 0)),
                "dvl_bl": bool(msg.status & (1 << 1)),
                "gps": bool(msg.status & (1 << 2)),
                "depth": bool(msg.status & (1 << 3)),
                "comp": bool(msg.status & (1 << 4)),
                "imu": bool(msg.status & (1 << 5)),
                "oas": bool(msg.status & (1 << 6)),
                "nav": bool(msg.status & (1 << 7)),
                "epuck": bool(msg.status & (1 << 8)),
                "abort": bool(msg.status & (1 << 9)),
                "press": bool(msg.status & (1 << 13)),
                "leak": bool(msg.status & (1 << 15))
            },

            # required: lat, lon
            # optional, but required for dash display: heading, roll, pitch, bat
            # optional, speed, uncertainty, alt, depth
            # all values must be floats
            "pose": {
                "lat": lat,  # float, decimal degrees
                "lon": lon,  # float, decimal degrees
                "alt": alt,  # float, m
                "depth": float(msg.depth) / 10.0,  # float, m [sent as 10*m]
                "heading": head,
            # float(msg.heading)*2.0,                    	# float, degrees, range: 0:360 [sent as 1/2 degrees 0..360]
                "pitch": float(msg.pitch) / 4.0,  # float, degrees, range: -180:180 [sent as 1/4*degrees]
                "roll": float(msg.roll) / 4.0  # float, degrees, range: -180:180 [sent as 1/4*degrees]
            },

            # All stat key-value pairs are optional. For no alerts, include empty dict, ie {}
            # optional, but required for dash display: bat
            # The keys can be anything, but keep them short for display.
            # The values can be anything, with the exception of bat, which must be an int
            "stat": {
                "bat": int(msg.charge),  # int, %, range: 0:100
                "waypt": msg.waypoint,
                "#imgs": msg.img_count
            }
        }

        platform_key = str(channel)

        # And POST the data
        # post_platformdata(platform_key, platform_data, "platform")
        self.update_platform_data(platform_key, platform_data)

    def run(self):
        self.lc.subscribe("AUVSTAT.*", self.float_status_handler)
        self.lc.subscribe("USBL_FIX.*", self.usbl_fix_handler)

        timeout = 1  # amount of time to wait, in seconds for subscriptions

        # Main loop just handling incoming LCM messages (unless self destruct)
        while not self.exitFlag:
            rfds, wfds, efds = select.select([self.lc.fileno()], [], [], timeout)
            if rfds:
                self.lc.handle()


# ----------------------------------END-LCM-THREAD------------------------------------- #


def usec_time():
    return int(time.time() * 1000000)


def sec_time(microtime):
    return microtime / 1000000


# --------------------------------------MAIN------------------------------------------- #

def Main():
    # get optional args
    args = get_args()

    # instantiate object
    acfrplatforms = LcmThread(maptracker_url=args["url"], maptracker_port=args["port"])

    # start threads
    acfrplatforms.start()

    # keep alive
    acfrplatforms.serve_forever()


# ----------------------------------END-MAIN------------------------------------------- #

if __name__ == "__main__":
    Main()
