
from platform_tools.platformdata import post_data
from time import sleep
import requests

settings = {
    "remote_url": "http://144.6.225.82",
    "remote_port": 80,
    "post_delay": 5,                        # seconds
    "data": [
        # platforms
        # {"type": "platform", "key": "test", "url": "http://localhost:8080/data/platformdata.api.Platform/test"},
        # {"type": "platform", "key": "AUVSTAT.SIRIUS", "url": "http://localhost:8080/data/platformdata.api.Platform/SIRIUS.AUVSTAT"},
        # {"type": "platform", "key": "USBL_FIX.SIRIUS", "url": "http://localhost:8080/data/platformdata.api.Platform/USBL_FIX.SIRIUS"},
        # {"type": "platform", "key": "AUVSTAT.HOLT", "url": "http://localhost:8080/data/platformdata.api.Platform/HOLT.AUVSTAT"},
        # {"type": "platform", "key": "USBL_FIX.HOLT", "url": "http://localhost:8080/data/platformdata.api.Platform/USBL_FIX.HOLT"},
        # {"type": "platform", "key": "AUVSTAT.NGA", "url": "http://localhost:8080/data/platformdata.api.Platform/NGA.AUVSTAT"},
        # {"type": "platform", "key": "AUVSTAT.WAMV", "url": "http://localhost:8080/data/platformdata.api.Platform/WAMV.AUVSTAT"},
        # {"type": "platform", "key": "uri.lfloat", "url": "http://localhost:8080/data/platformdata.api.Platform/PFLOAT.AUVSTAT"},
        {"type": "platform", "key": "uri.lfloat", "url": "http://localhost:8080/data/platformdata.api.Platform/FALKOR.USBL_FIX.PFLOAT"},
        {"type": "platform", "key": "whoi.slocum", "url": "http://localhost:8080/data/platformdata.api.Platform/whoi.slocum"},
        # {"type": "platform", "key": "umich.imorb", "url": "http://localhost:8080/data/platformdata.api.Platform/umich.imorb"},
        {"type": "platform", "key": "umich.iver", "url": "http://localhost:8080/data/platformdata.api.Platform/DROPIVER.AUVSTAT"},

        # ship
        # {"type": "platform", "key": "SHIP_STATUS.ORANGEBOX", "url": "http://localhost:8080/data/platformdata.api.Platform/ORANGEBOX.SHIP_STATUS"},
        {"type": "platform", "key": "SHIP_STATUS.FALKOR", "url": "http://localhost:8080/data/platformdata.api.Platform/FALKOR.SHIP_STATUS"},

        # missions
        # {"type": "mission", "key": "test", "url": "http://localhost:8080/get_mission/platformdata.api.Platform/test"}
        {"type": "mission", "key": "test", "url": "http://localhost:8080/get_mission/platformdata.api.Platform/umich.iver"}
    ]
}





target_data = {}

while True:
    for t in settings['data']:
        try:
            r = requests.get(t["url"])
            data = r.json()
            cache_key = t["type"]+"-"+t["key"]
            if cache_key in target_data:
                do_post = data["msgts"] > target_data[cache_key]
            else:
                do_post = True
            if do_post:
                post_data(t["key"], data, data_type=t["type"], maptracker_url=settings["remote_url"], maptracker_port=settings["remote_port"])
                target_data[cache_key] = data["msgts"]
        except Exception as e:
            print "*** ERROR: {} ({})! - {}".format(t["key"], t["type"], e)
    sleep(settings["post_delay"])