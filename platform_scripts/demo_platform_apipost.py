from time import sleep, time
from platform_tools.platformdata import APIPlatform, get_args
from platform_tools.generate_fake_data import get_fake_log_message, get_fake_mission, get_fake_platform_data
import thread


# Platform key: must be unique
platform_key = "test"


class APIDemoPlatform(APIPlatform):
    def __init__(self, **kwargs):
        #self.origin = [-33.8405831822, 151.2548891302]  # not needed for a real vehicle, this is just for fake data
        self.origin = [20.72404658, -156.66]  # default origin AU-AU channel, hawaii
        APIPlatform.__init__(self, **kwargs)

    def platformdata_update_thread(self, platform_key):
        """
        NOTE: This should be a thread that subscribes to a UDP feed or similar to receive nav updates. It should
        construct the nav data object as done here, but when new data is received.
        """
        while True:
            data = get_fake_platform_data(self.origin)
            self.update_platform_data(platform_key, data)
            sleep(1)

    def mission_update_thread(self, platform_key):
        """
        NOTE: THIS WOULD NORMALLY NOT BE DONE IN A THREAD LIKE THIS. IT SHOULD BE DONE ON DEMAND AS A NEW MISSION IS
        LOADED ONTO THE PLATFORM. DONE HERE IN A THREAD TO DEMONSTRATE IT UPDATING EVERY 30s
        """
        while True:
            mission_data = get_fake_mission(self.origin)
            self.update_platform_mission(platform_key, mission_data)
            sleep(30)

    def random_log_message_thread(self, platform_key):
        """
        NOTE: THIS WOULD NORMALLY NOT BE DONE IN A THREAD LIKE THIS. IT SHOULD BE DONE ON DEMAND WHEN YOU WANT TO POST A
        MESSAGE THROUGH TO THE UI. DONE HERE IN A THREAD TO DEMONSTRATE WORKING.
        """
        while True:
            msg_type, msg, next_message_delay = get_fake_log_message()
            self.log_platform_message(platform_key, msg_type, msg)
            sleep(next_message_delay)

    def start_threads(self, platform_key):
        thread.start_new_thread(platform.platformdata_update_thread, (platform_key,))
        thread.start_new_thread(platform.mission_update_thread, (platform_key,))
        thread.start_new_thread(platform.random_log_message_thread, (platform_key,))


if __name__ == "__main__":

    # get cli args
    args = get_args()

    # Instantiate class
    platform = APIDemoPlatform(maptracker_url=args["url"], maptracker_port=args["port"], command_server_port=args["command_port"])

    # Start data threads
    platform.start_threads(platform_key)

    # keep main thread alive
    platform.serve_forever()
